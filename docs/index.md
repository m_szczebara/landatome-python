# Landatome Python


!!! info "Partie Python de Landatome"
  
    Bienvenue sur ce site.<br>
    Vous y trouverez de quoi démarrer en Python ainsi que des exercices ciblant les programmes de Mathématiques du lycée.
    <br>
    Certaines notions, comme les listes seront abordées plus tôt que suggéré dans ces derniers. Cette liberté est prise car vous rencontrerez ces notions dans d'autres disciplines ou enseignements.
    <br>
    Vous pouvez revenir au site principal grâce à ce bouton :<br>
    [Landatome](https://adminlandatome.github.io/LANDATOME/){ .md-button target="_blank" rel="noopener" }

    


!!! info "Exercices Python : Utilisation de l'IDE sur le site"


    Pour la plupart des exercices il y a un IDE possédant 5 boutons dont les 2 principaux sont :

    <div class="py_mk_ide">
    <ul>
    <li> <button class="tooltip"><img src="pyodide-mkdocs/icons8-play-64.png"></button> pour <b>Lancer</b> le script (l'exécuter). </li>

    <li> <button class="tooltip"><img src="pyodide-mkdocs/icons8-check-64.png"></button> pour <b>Valider</b> votre code. <b>Il faut absolument cliquer sur ce bouton</b>, car il se peut que l'exécution ne pose pas de problème avec le bouton <b>Lancer</b> alors que votre code ne répond pas à tout ce qui est demandé dans l'exercice. 
    <br> Votre code est évalué à l'aide de tests cachés. Si tous les tests cachés sont réussis, le corrigé et éventuellement des remarques, s'affichent. 
    <br> ⚠ Parfois ce bouton n'existe pas, lorsqu'il faut juste exécuter un script, et qu'aucune validation n'est prévue.
    </li>
    </ul>
    </div>
    
    Les autres boutons sont :


    <div class="py_mk_ide">
    <ul>
    <li> Télécharger le script actuel : <button class="tooltip"><img src="pyodide-mkdocs/icons8-download-64.png"></button></li>

    <li> Téléverser un script local : <button class="tooltip"><img src="pyodide-mkdocs/icons8-upload-64.png"></button></li>

    <li> Recharger l'énoncé : <button class="tooltip"><img src="pyodide-mkdocs/icons8-restart-64.png"></button></li>

    <li> Sauvegarder en ligne le script actuel : <button class="tooltip"><img src="pyodide-mkdocs/icons8-save-64.png"></button></li>
    <br> Après avoir cliqué sur ce bouton, si vous changez de page, ou rafraichissez la page dans votre navigateur, vous retrouverez ce script. Cela ne permet pas de télécharger votre script (bouton <button class="tooltip"><img src="pyodide-mkdocs/icons8-download-64.png"></button> pour cela)
    
    </ul>
    </div>







