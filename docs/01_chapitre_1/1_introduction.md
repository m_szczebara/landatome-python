---
author : Thomas SZCZEBARA
title : I Introduction
---

??? note "Python n'est pas un animal"

    Le langage Python voit sa première version apparaitre en 1991. Son Auteur, le programmeur Guido van Rossum commence à le développer pendant les fêtes de Noël 1989 et comme il est fan de la série télévisée Monty Python's Flying Circus, il décide de baptiser son projet : Python.

??? note "mais nous allons quand même l'apprivoiser avec l'aide d'un certain EDI."

    Pour utiliser Python, nous aurons besoin d'un EDI (Environnement de Développement Intégré). L'un d'eux est directement disponible sur ce site, mais vous en rencontrerez d'autres comme Edupython et Thonny par exemple.<br>
    En anglais, on parle de IDE (Integrated Development Environment).



???+ "Pour cela, voici un exemple de l'EDI qui sera utilisé sur ce site"

    La première partie (avec les numéros) est **l'éditeur** la seconde (avec les ">>>") se nomme **console** ou encore **Shell**.
    Vous pouvez faire vos premiers essais ici. <br>
    Pensez à exécuter le script (avec le bouton **Lancer**)
    
    {{ IDE('scripts/fichier') }}

