---
author : Thomas SZCZEBARA
title : II Opérations, types, variables et affectations
---


??? note "Les opérations"

    <ul>
    <li>La somme de 1 et 2 : ```1 + 2```</li>
    <li>La différence de 7 et 3 : ```7 - 3```</li>
    <li>Le produit 4 par 5 : ```4 * 5``` </li>
    <li>Le quotient de 7 par 8 : ```7 / 8```</li>
    <li>L'élévation de 4 à la puissance 7  : ```4**7```</li>
    <li>En particulier, 3 au carré : ```3 ** 2```</li>
    <li>Le quotient dans la division Euclidienne de 45 par 7 : ```15 // 7```</li>
    <li>Le reste dans la division Euclidienne de 45 par 7 : ```45 % 7```</li>
    </ul>
    Testez-les dans l'EDI plus haut (dans la partie avec le prompt ">>>" obtenue après avoir exécuté le script).
    
??? note "La fonction intégrée ```print``` (première approche)"

    Si vous écrivez la commande ```1 + 2``` dans l'éditeur et que vous exécutez le script alors vous ne verrez rien apparaitre dans la console (la partie avec ">>>").<br>
    Pour voir le résultat, vous devez utiliser la fonction ```print``` et taper la commande : ```print(1+2) ```<br>
    Une fois l'exécution lancée, vous verrez apparaitre votre résultat dans la console.<br>
    Nous reparlerons de cette fonction au fur et mesure de nos besoins dans le cours...


??? note "Variables, types et affectations"

    Une **variable** peut être vue comme **une boite** dans laquelle on va déposer (on dira : **affecter**) une valeur d'une certaine nature (on dira : **type**).<br>
    La commande ``` a = 1 ``` affecte la valeur 1 qui est de type "entier" (``` int```) à la variable qui se nomme ```a``` <br>

???+ "Voyons cela avec un EDI (n'oubliez pas d'exécuter le script avec le bouton **Lancer**)"

    {{ IDE('scripts/variable_affectation_type_01') }}

???+ "Les différents types (n'oubliez pas d'exécuter le script avec le bouton **Lancer**)"

    {{ IDE('scripts/variable_affectation_type_02') }}

??? note "Nommer une variable"

    Dans ce qui précéde, nous avons donné des noms simples à nos variables : ```a``` ,```b``` etc... <br>
    C'est rarement une bonne idée, nous aurions mieux fait de les nommer de la façon suivante : <br>
    <ul>
    <li>```mon_entier``` à la place de ```a``` (surtout pas d'espace!)</li>
    <li>```mon_flottant``` à la place de ```b```</li>
    <li>```mon_booleen``` à la place de ```c```  (éviter les accents et autres caratères spéciaux, c'est mieux...)</li>
    <li>```une_chaine_01``` à la place de ```d``` (Ne pas commencer par un chiffre, mais on peut les utiliser ailleurs)</li>
    <li>```Une_liste``` à la place de ```b``` (On peut utiliser des majuscules)</li>
    </ul>

    On fera attention : ```Une_liste``` et ```une_liste``` sont des noms différents ! 

???+ "Réécrivez le script précédent avec les modifications ci-dessus<br> (n'oubliez pas d'exécuter le script avec le bouton **Lancer**)"

    {{ IDE('scripts/variable_affectation_type_03') }}
 
??? danger "Les premières erreurs à éviter"

    <ul>
    <li> Ne pas oublier d'encadrer vos chaines de caractères par des ``` " ``` ou des ``` ' ``` :  <br>
    ```"bonjour"``` est une chaine de caractères mais ```bonjour``` est le nom d'une variable.
    </li>
    <li> Les nombres décimaux s'écrivent avec point et non une virgule : <br>
    ````12.45``` est un nombre décimal (en fait un "flottant", mais la nuance n'est pas encore importante) mais ```12,45``` représente un couple de deux entiers : 12 et 45 (on parlera de "tuple" ).
    </li>
    <li> Une liste d'éléments, s'écrit entre crochets et ses éléments sont séparés par des virgules. </li>
    </ul>

???+ "Corrigez le script suivant afin de ne plus obtenir d'erreur lors de son exécution."

    {{ IDE('scripts/variable_affectation_type_04') }}

??? note "Faire des opérations avec des variables"

    Vous pouvez utiliser toutes les opérations que vous avez vues sur des variables à condition de faire attention à leur type : <br>
    ```python
    a = 5 
    b = 7
    c = a + b
    d = "bonjour "
    e = "Toto"
    f = d + e
    ```