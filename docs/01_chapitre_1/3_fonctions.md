---
author : Thomas SZCZEBARA
title : III Les fonctions
---


??? note "C'est quoi une fonction en Python ?"
    Une fonction est un bloc de code réutilisable qui effectue une tâche spécifique. En Python, on définit une fonction avec le mot-clé `def`.

???+ "Comment définir et appeler une fonction ?<br>Exemple n°1 <br>On définit ci-dessous la fonction 'dis_bonjour_a' qui prend comme argument 'machin' : <br>On l'appelle avec l'argument 'Toto'<br>```dis_bonjour_a('Toto')``` <br>(à taper dans la console après avoir appuyé sur le bouton Lancer)"

    {{ IDE('scripts/fonctions_01') }}
   
??? note "Le mot-clé `return`."
    La fonction définie au dessus a le mérite d'être simple et de fonctionner mais elle ne sert pas à grand chose...<br> Elle affiche quelque chose mais ne renvoie rien... <br> Le mot-clé `return` est utilisé uniquement dans le corps d'une fonction et sert justement à renvoyer un résultat. 

???+ "Exemple n°2 <br>Ecrivons autrement la fonction précédente :<br>On l'appelle avec l'argument 'Toto'<br>```dis_bonjour_a('Toto')``` <br>(à taper dans la console après avoir appuyé sur le bouton Lancer)"

    {{ IDE('scripts/fonctions_02') }}
    
??? note "Il y a une différence subtile entre ces deux fonctions"
    Si vous appelez la fonction depuis l'éditeur dans l'exemple n°1 alors vous aurez le résultat attendu dans la console.<br>Mais si   vous faites la même chose dans l'exemple n°2 alors rien ne se passe dans la console...<br>La fonction a bien renvoyé la chaine de caractères, mais vous n'avez pas demandé d'affichage donc... rien ne s'affiche. Pour obtenir un affichage depuis l'éditeur, il faut utiliser la fonction intégrée `print` : <br> ````print(dis_bonjour_a("Toto"))```

???+ "Exemple n°3 : <br> Nous voulons calculer la valeur de l'expression $A(x) = x^2-2x-4$ pour $x=0$, $x=5$ et $x=8$. <br>Pour cela nous allons utiliser une fonction...<br>(N'oubliez pas le bouton lancer)"

    {{ IDE('scripts/fonctions_03') }}

    