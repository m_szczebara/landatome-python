---
author : Thomas SZCZEBARA
title : IV Les instructions conditionnelles
---
??? note "Qu'est-ce qu'une condtion ?"
    Une condition est une expression logique dont le résultat est soit « vrai » soit « faux »

??? note "Comment construire une condition ?"
    Une condition est construite à l’aide d’opérateurs de comparaison :<br>▪ L’opérateur « égal à » noté `==` (sans espace)<br>▪ L’opérateur « différent de » noté `!=` ou `<>` (sans espace)<br>▪ Les opérateurs « inférieur à » ou « supérieur à » notés `<` et `>` <br>▪ Les opérateurs « inférieur ou égal à » ou « supérieur ou égal à » notés :<br> `<=` et `>=` <br>Lorsque la situation à tester est plus compliquée, il est possible de combiner plusieurs conditions grâce aux opérateurs logiques :<br>▪ « and » qui signifie « et » <br>▪ « or » qui signifie « ou »<br>▪ « not » qui signifie « non »
    
???+ "Un exemple de condition"

    {{ IDE('scripts/conditions_01') }} 

??? note "Les structures conditionnelles"
    Suivant la valeur d’une condition (vraie ou fausse), la fonction choisit les actions à réaliser. On parle de structures conditionnelles.

???+ "La structure conditionnelle `if` permet d’exécuter un bloc d’instruction lorsqu’une condition est vérifiée." 
    
    {{ IDE('scripts/conditions_02') }}

???+ "La structure conditionnelle `if ... else` permet d’exécuter un bloc d’instructions lorsqu’une condition est vérifiée et un autre bloc lorsqu’elle ne l’est pas."

    {{ IDE('scripts/conditions_03') }}

???+ "La structure conditionnelle `if ... elif ... else` permet de gérer plusieurs conditions. Si une condition n’est pas validée, la suivante est étudiée et ainsi de suite jusqu'à la dernière. En Python, « elif » est contraction de else if qui signifie « sinon si »"

    {{ IDE('scripts/conditions_04') }}

??? danger "Ne pas oublier les `:` en fin de ligne !
    