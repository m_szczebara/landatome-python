#Déclarons deux variables

distance = 50
acces = "autorisé"

#La condition suivante teste si
#la distance est strictement inférieure à 40
#et si l'accès est autorisé

print(distance < 40 and acces == "autorisé")

#print() n'est là que pour l'affichage de la condition

