# On définit une fonction utilisant la structure 
#contionnelle if
def peut_acceder(est_membre):
    reponse = "accès refusé"         #au départ l'accès est refusé
    if est_membre == "oui":          #on teste si la personne est membre
        reponse = "accès autorisé"   #si c'est le cas, l'accès est autorisé
    return reponse                   #on renvoie la réponse

#un premier appel avec "oui"
print("un premier appel")
print(peut_acceder('oui'))           #print ne sert qu'à officher ce que renvoie la fonction

#un deuxième appel avec "non"
print("un deuxième appel")
print(peut_acceder('non'))

#un troisième appel avec "Oui" au lieu de "oui"
print("un troisième appel")
print(peut_acceder('Oui'))
