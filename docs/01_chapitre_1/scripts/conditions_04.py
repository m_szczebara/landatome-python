#On définit une fonction utilisant la structure
#conditionnelle if ... elif ... else
def visibilite(statut):
    if statut == "secret" :
        reponse = "Seul les membres autorisés voient le groupe"
    elif statut == "fermé" :
        reponse = "Tout le monde voit le groupe mais pas les publications"
    else :
        reponse = "Tout le monde voit le groupe et les publications"
    return reponse

#un premier appel avec "oui"
print("un premier appel")
print(peut_acceder_bis('secret'))           #print ne sert qu'à officher ce que renvoie la fonction

#un deuxième appel avec "non"
print("un deuxième appel")
print(peut_acceder_bis('Secrte'))

#un troisième appel avec "Oui" au lieu de "oui"
print("un troisième appel")
print(peut_acceder_bis('fermé'))

