a = 1
print(a)
print(type(a)) #a contient un entier
b = 1.2
print(b)
print(type(b)) #b contient un flottant (un nombre décimal ou presque)
c = True 
print(c)
print(type(c)) #c contient un booléen (True ou False - majuscule pour la 1ere lettre obligatoire)
d = "Bonjour"
print(d)
print(type(d)) #d contient une chaine de caractères (string)
e = [1,2,3]
print(e)
print(type(e)) #e contient une liste 
