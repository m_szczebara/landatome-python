#On définit une fonction utilisant la structure
#conditionnelle if ... else
def peut_acceder_bis(est_membre):
    if est_membre == "oui":
        reponse = "accès autorisé"
    else :
        reponse = "accès refusé"
    return reponse

#un premier appel avec "oui"
print("un premier appel")
print(peut_acceder_bis('oui'))           #print ne sert qu'à officher ce que renvoie la fonction

#un deuxième appel avec "non"
print("un deuxième appel")
print(peut_acceder_bis('non'))

#un troisième appel avec "Oui" au lieu de "oui"
print("un troisième appel")
print(peut_acceder_bis('Oui'))
